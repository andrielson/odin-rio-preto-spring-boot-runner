package io.github.andrielson.odin.riopreto.publications;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@RequiredArgsConstructor
@Slf4j
@SpringBootApplication
public class OdinRioPretoPublicationsApplication implements CommandLineRunner {

    private final SearchAndSendUseCase searchAndSendUseCase;

    public static void main(String[] args) {
        SpringApplication.run(OdinRioPretoPublicationsApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        var localDate = LocalDate.parse("2021-10-01", DateTimeFormatter.ISO_LOCAL_DATE);
        searchAndSendUseCase.searchByDate(localDate);
    }
}
