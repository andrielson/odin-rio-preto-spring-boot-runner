package io.github.andrielson.odin.riopreto.publications;

public record Publication(String keyword, String code, String link) {
}
