package io.github.andrielson.odin.riopreto.publications.mailer;

import org.springframework.lang.NonNull;

public interface EmailMessage {
    @NonNull
    String getHtml();

    @NonNull
    String getText();

    @NonNull
    String getTo();

    @NonNull
    String getSubject();
}
