package io.github.andrielson.odin.riopreto.publications;

import io.github.andrielson.odin.riopreto.publications.subscribers.SubscribersRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@RequiredArgsConstructor
@Service
@Slf4j
public class SearchAndSendUseCase {

    private final SearchPublicationsUseCase searchPublicationsUseCase;
    private final SubscribersRepository subscribersRepository;

    public void searchByDate(@NonNull final LocalDate localDate) {
        final var searcher = new Searcher(searchPublicationsUseCase, localDate);
        subscribersRepository.findAll()
                .map(searcher::x)
//                .toList();
                .forEach(dto -> {
                    log.info("========> Resultado da busca:");
                    log.info("========> Email: {}", dto.email());
                    dto.publicationsByKeyword().forEach((keyword, publications) -> {
                        log.info("========> Palavra-chave: {}", keyword);
                        publications.forEach(publication -> log.info("========> Link: {}", publication.link()));
                    });
                });
    }
}
