package io.github.andrielson.odin.riopreto.publications;

import org.jsoup.Jsoup;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.stream.Stream;

@Service
public class SearchPublicationsUseCase {
    public Stream<Publication> searchForKeywordAndDate(@NonNull String keyword, @NonNull LocalDate localDate) {
        final var datePattern = "dd/MM/yyyy";
        final var linkPrefix = "https://www.riopreto.sp.gov.br/DiarioOficial/";
        final var formParams = new HashMap<String, String>(3);
        formParams.put("Diario!listar.action", "Buscar");
        formParams.put("diario.dataPublicacao", localDate.format(DateTimeFormatter.ofPattern(datePattern)));
        formParams.put("diario.palavraChave", keyword);
        try {
            return Jsoup.connect(linkPrefix + "Diario!listar.action")
                    .data(formParams)
                    .post()
                    .body()
                    .getElementsByAttributeValueStarting("href", "Diario!arquivo.action?diario.codPublicacao=")
                    .stream()
                    .map(element -> new Publication(keyword, element.attr("href").split("=")[1], linkPrefix + element.attr("href")));
        } catch (IOException e) {
            e.printStackTrace();
            return Stream.empty();
        }
    }
}
