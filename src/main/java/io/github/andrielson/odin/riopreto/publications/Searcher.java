package io.github.andrielson.odin.riopreto.publications;

import io.github.andrielson.odin.riopreto.publications.subscribers.Subscriber;
import io.github.andrielson.odin.riopreto.publications.subscribers.SubscriberPublicationsDto;
import org.springframework.lang.NonNull;

import java.time.LocalDate;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

public class Searcher {
    private final SearchPublicationsUseCase searchPublicationsUseCase;
    private final ConcurrentMap<String, List<Publication>> publicationsCache;
    private final LocalDate localDate;

    public Searcher(SearchPublicationsUseCase searchPublicationsUseCase, LocalDate localDate) {
        this.searchPublicationsUseCase = searchPublicationsUseCase;
        this.localDate = localDate;
        publicationsCache = new ConcurrentHashMap<>();
    }

    public @NonNull
    SubscriberPublicationsDto x(@NonNull final Subscriber subscriber) {
        return subscriber.keywords()
                .stream()
                .map(keyword -> {
                    if (!publicationsCache.containsKey(keyword)) {
                        publicationsCache.put(keyword, searchPublicationsUseCase.searchForKeywordAndDate(keyword, localDate).toList());
                    }
                    return publicationsCache.get(keyword)
                            .stream()
                            .collect(Collectors.groupingBy(Publication::keyword));
                })
                .map(pubs -> new SubscriberPublicationsDto(subscriber.email(), pubs))
                .findFirst()
                .orElseThrow();
    }
}
