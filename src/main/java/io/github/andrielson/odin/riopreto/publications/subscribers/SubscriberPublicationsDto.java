package io.github.andrielson.odin.riopreto.publications.subscribers;

import io.github.andrielson.odin.riopreto.publications.Publication;

import java.util.List;
import java.util.Map;

public record SubscriberPublicationsDto(String email, Map<String, List<Publication>> publicationsByKeyword) {
}
