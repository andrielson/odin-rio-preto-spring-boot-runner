package io.github.andrielson.odin.riopreto.publications.subscribers;

import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

@Repository
@Slf4j
public class SubscribersRepository {
    private final HttpClient httpClient = HttpClient.newBuilder()
            .version(HttpClient.Version.HTTP_2)
            .build();

    public @NonNull
    Stream<Subscriber> findAll() {
        var uri = URI.create("https://odin-rio-preto-web.vercel.app/api/subscribers");
        var httpRequest = HttpRequest.newBuilder(uri)
                .GET()
                .build();
        HttpResponse<String> httpResponse;
        try {
            httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            return Stream.empty();
        }
        return Stream.of(httpResponse.body())
                .filter(this::isValidJsonArray)
                .map(JSONArray::new)
                .map(this::jsonArrayToJsonObjectList)
                .flatMap(List::parallelStream)
                .map(this::jsonObjectToSubscriber);
    }

    private boolean isValidJsonArray(@NonNull String body) {
        return body.startsWith("[") && body.endsWith("]");
    }

    private @NonNull
    List<JSONObject> jsonArrayToJsonObjectList(@NonNull JSONArray jsonArray) {
        var size = jsonArray.length();
        var jsonObjectList = new ArrayList<JSONObject>(size);
        for (var i = 0; i < size; i++) jsonObjectList.add(jsonArray.getJSONObject(i));
        return jsonObjectList;
    }

    private @NonNull
    Subscriber jsonObjectToSubscriber(@NonNull JSONObject jsonObject) {
        var email = jsonObject.getString("email");
        var keywords = jsonObject.getJSONArray("keywords")
                .toList()
                .stream()
                .map(String.class::cast)
                .toList();
//        return new Subscriber(email, keywords);
        return new Subscriber(email, List.of("semae"));
    }
}
