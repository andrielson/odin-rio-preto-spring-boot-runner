package io.github.andrielson.odin.riopreto.publications.subscribers;

import java.util.List;

public record Subscriber(String email, List<String> keywords) {
}
